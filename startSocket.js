var db = require('./configuration/database/db_sql')
var dbSyNotification = require('./model/synotification');
const log = require('./configuration/log/log');
var moment = require('moment');
const config = require('./configuration/config');
const fs = require("fs");

var http = require('http');
var https = require('https');

let httpMessage = '';
if (config.app.socketSsl) {

    privateKeyFile = config.app.privateKeyFile;
    certificateFile = config.app.certificateFile;
    caCertificateFile = config.app.caCertificateFile;

    if (!fs.existsSync(privateKeyFile) || !fs.existsSync(certificateFile) || !fs.existsSync(caCertificateFile)) {
        console.error("Error privateKeyFile, caCertificateFile and certificateFile not exist");
        return;
    }

    const privateKey = fs.readFileSync(privateKeyFile, { encoding: 'utf8' }, function (err, data) {
        console.error("Error privateKey: " + data);
    });

    const certificate = fs.readFileSync(certificateFile, { encoding: 'utf8' }, function (err, data) {
        console.error("Error certificate: " + data);
    });

    const caFile = fs.readFileSync(caCertificateFile, { encoding: 'utf8' }, function (err, data) {
        console.error("Error CA certificate: " + data);
    });

    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: [caFile],
        rejectUnauthorized: false
    };

    server = https.createServer(credentials);
    httpMessage = "https ";
} else {
    server = http.createServer();
}

const io = require('socket.io')(server, {
    cors: {
        methods: ["GET", "POST"],
        allowedHeaders: ["Origin,X-Requested-With,Content-Type,Accept,content-type,Authorization"]
    }
});
let socketId = null;
var intervalID;

const startTimer = function (time) {
    stopTimer();
    intervalID = setInterval(function () {
        getNotification();
    }, time);

    console.log('Timer service started. (interval -> ' + time + ')');
};

const stopTimer = function () {
    if (intervalID) {
        clearInterval(intervalID);
        console.log('Timer service stopped.');
    }
};


const getNotification = async function () {
    //** Execute Notification */
    sql = "SELECT distinct refId, module, event, member_id, formCode, data, createBy FROM synotification WHERE module='Socket' AND active = 'Y' AND status='WAIT'";
    db.query(sql, { type: db.QueryTypes.SELECT }).then(async function (result) {

        if (result.length > 0) {
            console.log("*New notification!");
        }

        //** Update DONE Notification */
        sql = "SELECT * FROM synotification WHERE module='Socket' AND active = 'Y' AND status='WAIT'";
        await db.query(sql, { type: db.QueryTypes.SELECT }).then(async function (result) {
            if (result.length > 0) {
                for (let index in result) {
                    var syNotification = result[index];
                    await dbSyNotification.update({ status: "DONE", updateBy: "System", updateDate: new Date() }, { where: { notificationId: syNotification.notificationId } }).catch(function (err) {
                        console.error(err);
                        log.error(err.stack);
                    });
                    console.log("  --> " + moment(new Date()).format('YYYY-MM-DD HH:mm') + " Execute notification Id: " + syNotification.notificationId);

                    let findNotification = await db.query("SELECT * FROM synotification WHERE notificationId = " + syNotification.notificationId, { type: db.QueryTypes.SELECT }).catch(err => { console.error(err); throw err; });
                    findNotification.socketId = socketId;
                    io.sockets.emit('execute', findNotification[0]);
                }
            }
        }).catch(function (err) {
            console.error(err);
            log.error(err.stack);
        });
    })
}


io.on('connection', socket => {
    socketId = socket.id;
    console.log("Socket Id Register " + socket.id);
    startTimer((1 * 1000));

    socket.on('register', data => {
        socket.emit('register', data);
    });
    socket.on('disconnect', () => { /* … */ });

});



server.listen(config.app.socketPort, function () {
    console.log(httpMessage + "Socket is running on port " + config.app.socketPort);
});