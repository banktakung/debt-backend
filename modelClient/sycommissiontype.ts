export class sycommissiontype {
    typeId: number;
    name: string;
    value: number;
    active: string;
    createBy: string;
    createDate: Date;
    updateBy: string;
    updateDate: Date;
};