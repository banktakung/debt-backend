var db = require('../configuration/database/db_sql')
const Sequelize = require('sequelize');
const trpaid = db.define('trpaid', {
  payId: { primaryKey: true, autoIncrement: true, type: Sequelize.INTEGER,  allowNull: false },
  companyContractNumber: {  type: Sequelize.STRING(25),  allowNull: true },
  payment: {  type: Sequelize.INTEGER,  allowNull: true },
  payDate: {  type: Sequelize.DATE,  allowNull: true },
  remark: {  type: Sequelize.TEXT,  allowNull: true },
  active: {  type: Sequelize.STRING(1),  allowNull: true },
  createBy: {  type: Sequelize.STRING(50),  allowNull: true },
  createDate: {  type: Sequelize.DATE,  allowNull: true },
  updateBy: {  type: Sequelize.STRING(50),  allowNull: true },
  updateDate: {  type: Sequelize.DATE,  allowNull: true },
}, { tableName: 'trpaid' });
module.exports = trpaid;