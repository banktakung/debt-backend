var db = require('../configuration/database/db_sql')
const Sequelize = require('sequelize');
const syNotification = db.define('synotification', {
  notificationId: { primaryKey: true, autoIncrement: true, type: Sequelize.INTEGER,  allowNull: false },
  refId: {  type: Sequelize.STRING(50),  allowNull: true },
  module: {  type: Sequelize.STRING(50),  allowNull: true },
  formCode: {  type: Sequelize.STRING(255),  allowNull: true },
  event: {  type: Sequelize.STRING(200),  allowNull: true },
  data: {  type: Sequelize.TEXT,  allowNull: true },
  member_id: {  type: Sequelize.INTEGER,  allowNull: true },
  status: {  type: Sequelize.STRING(50),  allowNull: true },
  active: {  type: Sequelize.STRING(1),  allowNull: true },
  createBy: {  type: Sequelize.STRING(50),  allowNull: true },
  createDate: {  type: Sequelize.DATE,  allowNull: true },
  updateBy: {  type: Sequelize.STRING(50),  allowNull: true },
  updateDate: {  type: Sequelize.DATE,  allowNull: true },
}, { tableName: 'synotification' });
module.exports = syNotification;