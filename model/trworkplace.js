var db = require('../configuration/database/db_sql')
const Sequelize = require('sequelize');
const trworkplace = db.define('trworkplace', {
  workplaceId: { primaryKey: true, autoIncrement: true, type: Sequelize.INTEGER,  allowNull: false },
  idcard: {  type: Sequelize.STRING(20),  allowNull: true },
  placeName: {  type: Sequelize.STRING(200),  allowNull: true },
  address: {  type: Sequelize.TEXT,  allowNull: true },
  province: {  type: Sequelize.STRING(200),  allowNull: true },
  amphure: {  type: Sequelize.STRING(200),  allowNull: true },
  district: {  type: Sequelize.STRING(200),  allowNull: true },
  zipCode: {  type: Sequelize.STRING(20),  allowNull: true },
  telephone1: {  type: Sequelize.STRING(20),  allowNull: true },
  telephone2: {  type: Sequelize.STRING(20),  allowNull: true },
  telephone3: {  type: Sequelize.STRING(20),  allowNull: true },
  startDate: {  type: Sequelize.STRING(200),  allowNull: true },
  active: {  type: Sequelize.STRING(1),  allowNull: true },
  createBy: {  type: Sequelize.STRING(50),  allowNull: true },
  createDate: {  type: Sequelize.DATE,  allowNull: true },
  updateBy: {  type: Sequelize.STRING(50),  allowNull: true },
  updateDate: {  type: Sequelize.DATE,  allowNull: true },
}, { tableName: 'trworkplace' });
module.exports = trworkplace;