var db = require('../configuration/database/db_sql')
const Sequelize = require('sequelize');
const trdocument = db.define('trdocument', {
  documentId: { primaryKey: true, autoIncrement: true, type: Sequelize.INTEGER,  allowNull: false },
  documentTypeId: { primaryKey: true, type: Sequelize.INTEGER,  allowNull: false },
  debtCollectionNumber: { primaryKey: true, type: Sequelize.INTEGER,  allowNull: false },
  fileType: {  type: Sequelize.STRING(200),  allowNull: true },
  fileName: {  type: Sequelize.STRING(200),  allowNull: true },
  filePath: {  type: Sequelize.STRING(200),  allowNull: true },
  remark: {  type: Sequelize.TEXT,  allowNull: true },
  active: { primaryKey: true, type: Sequelize.STRING(1),  allowNull: false },
  createBy: {  type: Sequelize.STRING(50),  allowNull: true },
  createDate: {  type: Sequelize.DATE,  allowNull: true },
  updateBy: {  type: Sequelize.STRING(50),  allowNull: true },
  updateDate: {  type: Sequelize.DATE,  allowNull: true },
}, { tableName: 'trdocument' });
module.exports = trdocument;