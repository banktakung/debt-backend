var db = require('../configuration/database/db_sql')
const Sequelize = require('sequelize');
const mscustomercontact = db.define('mscustomercontact', {
  phoneId: { primaryKey: true, autoIncrement: true, type: Sequelize.INTEGER, allowNull: false },
  idcard: { type: Sequelize.STRING(20), allowNull: true },
  phoneNumber: { type: Sequelize.STRING(20), allowNull: true },
  status: { type: Sequelize.STRING(20), allowNull: true },
  active: { type: Sequelize.STRING(1), allowNull: true },
  createBy: { type: Sequelize.STRING(50), allowNull: true },
  createDate: { type: Sequelize.DATE, allowNull: true },
  updateBy: { type: Sequelize.STRING(255), allowNull: true },
  updateDate: { type: Sequelize.DATE, allowNull: true },
}, { tableName: 'mscustomercontact' });
module.exports = mscustomercontact;