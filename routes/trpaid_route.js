var express = require('express');
var trpaid_route = express.Router();
var cors = require('cors');

/** controller */
var saveController = require('../controller/trpaid/save');
var getController = require('../controller/trpaid/get');

/** route */
trpaid_route.get('/findAll', getController.findAll);
trpaid_route.get('/findByPk', getController.findByPk);
trpaid_route.get('/findAllByCompanyContact', getController.findAllByCompanyContact);
trpaid_route.post('/save', cors({ origin: true }), saveController.save);

module.exports = trpaid_route;