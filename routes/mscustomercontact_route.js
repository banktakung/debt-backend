var express = require('express');
var mscustomercontact_route = express.Router();
var cors = require('cors');

/** controller */
var saveController = require('../controller/mscustomercontact/save');
var getController = require('../controller/mscustomercontact/get');

/** route */
mscustomercontact_route.get('/findAll', getController.findAll);
mscustomercontact_route.get('/findByPk', getController.findByPk);
mscustomercontact_route.get('/findByIdCard', getController.findByIdCard);
mscustomercontact_route.get('/getDebtContact', getController.getDebtContact);
mscustomercontact_route.post('/save', cors({ origin: true }), saveController.save);
mscustomercontact_route.post('/saveByPhoneNumber', cors({ origin: true }), saveController.saveByPhoneNumber);

module.exports = mscustomercontact_route;