var express = require('express');
var trworkplace_route = express.Router();
var cors = require('cors');

/** controller */
var saveController = require('../controller/trworkplace/save');
var getController = require('../controller/trworkplace/get');

/** route */
trworkplace_route.get('/findAll', getController.findAll);
trworkplace_route.get('/findByPk', getController.findByPk);
trworkplace_route.get('/findByIdCard', getController.findByIdCard);
trworkplace_route.post('/save', cors({ origin: true }), saveController.save);

module.exports = trworkplace_route;