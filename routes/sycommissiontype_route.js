var express = require('express');
var sycommissiontype_route = express.Router();
var cors = require('cors');

/** controller */
var saveController = require('../controller/sycommissiontype/save');
var getController = require('../controller/sycommissiontype/get');

/** route */
sycommissiontype_route.get('/findAll', getController.findAll);
sycommissiontype_route.get('/findByPk', getController.findByPk);
sycommissiontype_route.post('/save', cors({ origin: true }), saveController.save);

module.exports = sycommissiontype_route;