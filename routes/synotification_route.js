var express = require('express');
var synotification_route = express.Router();
var cors = require('cors');

/** controller */
var saveController = require('../controller/synotification/save');
var getController = require('../controller/synotification/get');

/** route */
synotification_route.get('/findAll', getController.findAll);
synotification_route.get('/findByPk', getController.findByPk);
synotification_route.post('/save', cors({ origin: true }), saveController.save);

module.exports = synotification_route;