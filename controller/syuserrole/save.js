var serviceResult = require('../../model/serviceResult');
var db_sql = require('../../configuration/database/db_sql');
const Sequelize = require('sequelize');
const log = require('../../configuration/log/log');
const Op = Sequelize.Op;
const dbManager = require('../../service/dbManager');
const syuserrole = require('../../model/syuserrole');


const save = async (req, res, next) => {
    try {
        var data = req.body;
        var where = { userRoleId: (data['userRoleId']) ? data['userRoleId'] : 0 };
        await dbManager.createOrUpdate(syuserrole, data, where).then(result => {
            serviceResult.value = result;
            serviceResult.code = 200;
            serviceResult.status = "Success";
            serviceResult.text = "Success";
            res.json(serviceResult);
        }).catch(err => console.error(err));
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const bulkCreate = async (req, res, next) => {
    try {
        await syuserrole.update({ active: 'N' }, { where: { username: req.body[0]?.username } }).catch(err => {
            console.error("code : 716ff472-8e26-44de-9f1c-df0abe756b9e");
            throw err;
        })
        await syuserrole.bulkCreate(req.body).catch(err => {
            console.error("code : adc49a62-a039-45f2-ae0f-4e6ab6f330cf");
            throw err;
        });
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

module.exports = { save, bulkCreate }