var serviceResult = require('../../model/serviceResult');
var db_sql = require('../../configuration/database/db_sql');
const Sequelize = require('sequelize');
const log = require('../../configuration/log/log');
const Op = Sequelize.Op;
const dbManager = require('../../service/dbManager');
const trdebtcollection = require('../../model/trdebtcollection');
const trproperty = require('../../model/trproperty');
const mscustomer = require('../../model/mscustomer');
const trdebtrelation = require('../../model/trdebtrelation');
const dayjs = require('dayjs');
const trfollow = require('../../model/trfollow');
const mscustomercontact = require('../../model/mscustomercontact');


const save = async (req, res, next) => {
    try {
        var data = req.body;
        var where = { debtCollectionNumber: (data['debtCollectionNumber']) ? data['debtCollectionNumber'] : null };
        await dbManager.createOrUpdate(trdebtcollection, data, where).then(result => {
            serviceResult.value = result;
            serviceResult.code = 200;
            serviceResult.status = "Success";
            serviceResult.text = "Success";
            res.json(serviceResult);
        }).catch(err => console.error(err));
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const importFromExcel = async (req, res, next) => {
    try {
        let data = req.body;
        let cnt = 0;
        for (let obj of data) {
            delete obj.deliveryDate1;
            delete obj.deliveryDate2;

            obj.active = "Y";
            obj.createdBy = global.authentication.username;
            obj.createDate = new Date();
            obj.updatedBy = global.authentication.username;
            obj.updateDate = new Date();
            var where = { fname: (obj.fname) ? obj.fname : null, companyContractNumber: (data['companyContractNumber']) ? data['companyContractNumber'] : null };
            await dbManager.createOrUpdate(trdebtcollection, obj, where).then(async result => {
                if (+obj?.idcard) {
                    let dataCustomer = {};
                    dataCustomer.idcard = obj?.idcard;
                    dataCustomer.customerCode = obj?.customerNumber;
                    dataCustomer.fname = obj?.fname;
                    dataCustomer.lname = obj?.lname;
                    dataCustomer.active = 'Y';
                    dataCustomer.createdBy = global.authentication.username;
                    dataCustomer.createDate = new Date();
                    dataCustomer.updatedBy = global.authentication.username;
                    dataCustomer.updateDate = new Date();
                    await dbManager.createOrUpdate(mscustomer, dataCustomer, { idcard: (obj?.idcard) ? obj?.idcard : null, customerCode: (obj.customerNumber) ? obj.customerNumber : null }).then(result => {
                    });

                    let dataRelation = {};
                    dataRelation.idcard = obj?.idcard;
                    dataRelation.companyContractNumber = result?.item?.companyContractNumber;
                    if (obj?.isMain == 'ผู้กู้ร่วม') {
                        dataRelation.isMain = 'N';
                    } else {
                        dataRelation.isMain = 'Y';
                    }
                    dataRelation.active = "Y";
                    dataRelation.createBy = global.authentication.username;
                    dataRelation.createDate = new Date();
                    dataRelation.updateBy = global.authentication.username;
                    dataRelation.updateDate = new Date();
                    await dbManager.createOrUpdate(trdebtrelation, dataRelation, { idcard: (dataRelation?.idcard) ? dataRelation?.idcard : null, companyContractNumber: (dataRelation?.companyContractNumber) ? dataRelation?.companyContractNumber : null, isMain: (obj?.isMain) ? obj?.isMain : null }).then(result => {
                    });

                    let dataProperty = {};
                    dataProperty.idcard = obj?.idcard;
                    dataProperty.domicileProperty = obj?.domicileProperty;
                    dataProperty.provincialOffice = obj?.provincialOffice;
                    dataProperty.dateSearch = obj?.dateSearch;
                    dataProperty.result = obj?.domicilePropertyFound;
                    dataProperty.detail = obj?.detail;
                    dataProperty.active = 'Y';
                    dataProperty.createdBy = global.authentication.username;
                    dataProperty.createDate = new Date();
                    dataProperty.updatedBy = global.authentication.username;
                    dataProperty.updateDate = new Date()
                    await dbManager.createOrUpdate(trproperty, dataProperty, { idcard: (obj?.idcard) ? obj?.idcard : null, domicileProperty: (dataProperty?.domicileProperty) ? dataProperty?.domicileProperty : null, provincialOffice: (dataProperty?.provincialOffice) ? dataProperty?.provincialOffice : null }).catch(err => {
                        console.error("code : d4e48ec3-d4c6-4616-8882-da90a461fc07");
                        throw err;
                    });

                    let dataFollow = {};
                    dataFollow.companyContractNumber = result?.item?.companyContractNumber;
                    dataFollow.followDate = obj?.followDate;
                    dataFollow.followDetail = obj?.followDetail;
                    dataFollow.active = 'Y';
                    dataFollow.createBy = global.authentication.username;
                    dataFollow.createDate = new Date();
                    dataFollow.updateBy = global.authentication.username;
                    dataFollow.updateDate = new Date();
                    await dbManager.createOrUpdate(trfollow, dataFollow, { companyContractNumber: (result?.item?.companyContractNumber) ? result?.item?.companyContractNumber : null, followDate: (obj?.followDate) ? obj?.followDate : null, followDetail: (obj?.followDetail) ? obj?.followDetail : null }).catch(err => {
                        console.error("code : 720ab110-0b5d-4f16-b1f2-90575d4c5f21");
                        throw err;
                    });

                    // Phone List
                    if (obj?.phoneList) {
                        if (obj?.phoneList.toString().includes(",")) {
                            let phoneList = obj?.phoneList?.replace(" ", "").split(',');
                            for (let phone of phoneList) {
                                let phoneData = {};
                                phoneData.idcard = obj?.idcard;
                                phoneData.phoneNumber = phone;
                                phoneData.active = 'Y';
                                phoneData.createBy = global.authentication.username;
                                phoneData.createDate = new Date();
                                phoneData.updateBy = global.authentication.username;
                                phoneData.updateDate = new Date();
                                await dbManager.createOrUpdate(mscustomercontact, phoneData, { idcard: (obj?.idcard) ? obj?.idcard : null, phoneNumber: (phone) ? phone : null }).catch(err => {
                                    console.error("code : 4aa9bbcf-cc93-4a48-88dc-54b8b0806cc1");
                                    throw err;
                                })
                            }
                        } else {
                            let phoneData = {};
                            phoneData.idcard = obj?.idcard;
                            phoneData.phoneNumber = obj?.phoneList;
                            phoneData.active = 'Y';
                            phoneData.createBy = global.authentication.username;
                            phoneData.createDate = new Date();
                            phoneData.updateBy = global.authentication.username;
                            phoneData.updateDate = new Date();
                            await dbManager.createOrUpdate(mscustomercontact, phoneData, { idcard: (obj?.idcard) ? obj?.idcard : null, phoneNumber: (obj?.phoneList) ? obj?.phoneList : null }).catch(err => {
                                console.error("code : 4aa9bbcf-cc93-4a48-88dc-54b8b0806cc1");
                                throw err;
                            })
                        }
                    }
                }


                cnt = cnt + 1;
                console.log("before:", cnt);
                if (cnt == data.length) {
                    console.log("after:", cnt);
                    serviceResult.code = 200;
                    serviceResult.status = "Success";
                    serviceResult.text = "Success";
                    res.json(serviceResult);
                }
            }).catch(err => console.error(err));

        }

    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

module.exports = { save, importFromExcel }