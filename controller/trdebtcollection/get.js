var serviceResult = require('../../model/serviceResult');
var db_sql = require('../../configuration/database/db_sql');
const Sequelize = require('sequelize');
const log = require('../../configuration/log/log');
const trdebtcollection = require('../../model/trdebtcollection');
const trdebtrelation = require('../../model/trdebtrelation');
const msdebtstatus = require('../../model/msdebtstatus');
const mscustomercontact = require('../../model/mscustomercontact');
const syuser = require('../../model/syuser');
const Op = Sequelize.Op;
let dayjs = require('dayjs')

const findAll = async (req, res, next) => {
    try {
        let result = await trdebtcollection.findAll({
            where: {
                active: 'Y',
                traceBy: null,
                status: null
            }
        });
        serviceResult.value = result;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const findByPk = async (req, res, next) => {
    try {
        let value = req.query.value;
        let result = await trdebtcollection.findByPk(value);
        serviceResult.value = result;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const findAllTrace = async (req, res, next) => {
    try {
        const result = await trdebtcollection.findAll({
            where: {
                active: 'Y',
                traceBy: {
                    [Op.not]: null
                }
            }
        }).catch(err => {
            console.error("code : f348592f-febb-42e1-b59c-757b6dfa7594");
            throw err;
        });

        serviceResult.value = result;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const findMyTrace = async (req, res, next) => {
    try {
        let status = req.query.status;
        let username = global.authentication.username;
        if (username) {
            const result = await trdebtcollection.findAll({
                where: {
                    active: 'Y',
                    traceBy: username,
                    status: status
                }
            }).catch(err => {
                console.error("code : 890e8fe8-7ebd-4239-9bbd-6addbb4b5ca2");
                throw err;
            });

            serviceResult.value = result;
            serviceResult.code = 200;
            serviceResult.status = "Success";
            serviceResult.text = "Success";
            res.json(serviceResult);
        } else {
            serviceResult.code = 500;
            serviceResult.status = "Error";
            serviceResult.text = "Error: Invalid Token.";
            res.json(serviceResult);
        }
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const findByStatus = async (req, res, next) => {
    try {
        let status = req.query.statusCode;
        let username = req.query.username;

        let where = {};
        if (status) where.status = status;
        if (username) where.traceBy = username;
        where.active = "Y";

        const result = await trdebtcollection.findAll({
            where: where,
            include: [msdebtstatus, mscustomercontact, syuser],
            group: ['companyContractNumber']
        })

        // let sql = "";
        // sql += " SELECT";
        // sql += " 	*,";
        // sql += " 	( SELECT nextFollowDate FROM trfollow WHERE trfollow.debtCollectionNumber = trdebtcollection.debtCollectionNumber ORDER BY followId DESC LIMIT 1 ) AS followDate, ";
        // sql += " 	( SELECT paymentType FROM trfollow WHERE trfollow.debtCollectionNumber = trdebtcollection.debtCollectionNumber ORDER BY followId DESC LIMIT 1 ) AS paymentType, ";
        // sql += " 	( SELECT numberOfMonth FROM trfollow WHERE trfollow.debtCollectionNumber = trdebtcollection.debtCollectionNumber ORDER BY followId DESC LIMIT 1 ) AS numberOfMonth, ";
        // sql += " 	( SELECT amount FROM trfollow WHERE trfollow.debtCollectionNumber = trdebtcollection.debtCollectionNumber ORDER BY followId DESC LIMIT 1 ) AS totalPayAmount, ";
        // sql += " 	( SELECT payPerMonth FROM trfollow WHERE trfollow.debtCollectionNumber = trdebtcollection.debtCollectionNumber ORDER BY followId DESC LIMIT 1 ) AS payPerMonth, ";
        // sql += " 	( SELECT name FROM syuser WHERE syuser.username = trdebtcollection.traceBy) AS traceName, ";
        // sql += "    ( SELECT	COUNT(*) FROM	trdebtrelation tdl	INNER JOIN mscustomercontact mcc ON mcc.idcard = tdl.idcard WHERE	tdl.active = 'Y' 	AND tdl.debtCollectionNumber = trdebtcollection.debtCollectionNumber ) AS maxContact,";
        // sql += "    ( SELECT	COUNT(*) FROM	trdebtrelation tdl	INNER JOIN mscustomercontact mcc ON mcc.idcard = tdl.idcard AND mcc.status = 'WRONGNUMBER'  WHERE	tdl.active = 'Y' 	AND tdl.debtCollectionNumber = trdebtcollection.debtCollectionNumber ) AS disableContact,";
        // sql += " 	( SELECT statusName FROM msdebtstatus WHERE msdebtstatus.statusCode = trdebtcollection.status) AS statusName ";
        // sql += " FROM";
        // sql += " 	trdebtcollection ";
        // sql += " WHERE";
        // sql += " 	active = 'Y' ";
        // if (status) {
        //     sql += " 	AND status = '" + status + "' ";
        // }
        // if (username) {
        //     sql += " 	AND traceBy = '" + username + "'";
        // }
        // if (searchTxt) {
        //     sql += "    AND (companyContractNumber LIKE '%" + searchTxt + "%')";
        // }
        // sql += " LIMIT 100";
        // let result = await db_sql.query(sql, { type: Sequelize.QueryTypes.SELECT });


        // let count = 0;
        // for (let debt of result) {
        //     debt.relationship = [];
        //     let sql = "";
        //     sql += " SELECT";
        //     sql += " 	* ";
        //     sql += " FROM";
        //     sql += " 	trdebtrelation";
        //     sql += " 	INNER JOIN mscustomer ON mscustomer.idcard = trdebtrelation.idcard";
        //     sql += " WHERE trdebtrelation.active = 'Y'";
        //     sql += " AND trdebtrelation.debtCollectionNumber = '" + debt.debtCollectionNumber + "'";
        //     let data = await db_sql.query(sql, { type: Sequelize.QueryTypes.SELECT });
        //     debt.relationship = data;
        //     count = count + 1;
        // }


        // if (count === result.length) {
        serviceResult.value = result;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
        // }
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const getComission = async (req, res, next) => {
    try {

    } catch (err) {

    }
}
const dashboardSummary = async (req, res, next) => {
    try {
        let summaryStatus = {};
        summaryStatus.week = [];
        summaryStatus.month = [];
        summaryStatus.year = [];

        //ใช้สำหรับ updateDate
        summaryStatus.summaryYear = [];
        summaryStatus.summaryWeek = [];
        summaryStatus.summaryMonth = [];

        let year = (req.query.year) ? req.query.year : dayjs(new Date()).format("YYYY");
        let months = (req.query.month) ? req.query.month : dayjs(new Date()).format("MM");
        let days = (req.query.days) ? req.query.days : dayjs(new Date()).format("DD");
        let date = new Date(year, months, 0).getDate();
        //week
        var first = (new Date()).getDate() - (new Date()).getDay();
        var firstday = dayjs(new Date((new Date()).setDate(first)).toUTCString()).format("YYYY-MM-DD");
        var lastday = dayjs(new Date((new Date()).setDate(first + 6)).toUTCString()).format("YYYY-MM-DD");

        const month = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
        const day = ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"];


        const debtStatus = await msdebtstatus.findAll({
            where: {
                active: 'Y'
            }
        });

        for (let obj of debtStatus) {

            //รายงานประจำปี

            sql = "";
            sql += " SELECT MONTH(updateDate) name, COUNT(updateDate) value FROM trdebtcollection";
            sql += " WHERE active = 'Y' AND YEAR(updateDate) = '" + year + "' ";
            sql += " AND status = '" + obj.statusName + "'";
            sql += " GROUP BY MONTH(updateDate);"
            let dashboardYear = await db_sql.query(sql, { type: Sequelize.QueryTypes.SELECT });

            let yearSummary = {};
            yearSummary.name = obj.statusName;
            yearSummary.series = [];

            for (let i = 0; i < month.length; i++) {
                let months = i + 1;
                let dataMonth = {};
                dataMonth.name = month[i];
                dataMonth.value = 0;
                for (let sum of dashboardYear) {
                    if (sum.name == months) {
                        dataMonth.value = sum.value;
                    }
                }
                yearSummary.series.push(dataMonth);
            }

            summaryStatus.year.push(yearSummary);

            //รายงานประจำเดือน

            sqlMonth = "";
            sqlMonth += " SELECT DAY(updateDate) name, COUNT(updateDate) value FROM trdebtcollection";
            sqlMonth += " WHERE active = 'Y' AND MONTH(updateDate) = '" + months + "' ";
            sqlMonth += " AND status = '"+obj.statusName+"'";
            sqlMonth += " GROUP BY DAY(updateDate);"
            let dashboardMonth = await db_sql.query(sqlMonth, { type: Sequelize.QueryTypes.SELECT });

            let monthSummary = {};
            monthSummary.name = obj.statusName;
            monthSummary.series = [];

            for (let i = 0; i < date; i++) {
                let months = i + 1;
                let dataMonth = {};
                dataMonth.name = i + 1;
                dataMonth.value = 0;
                for (let sum of dashboardMonth) {
                    if (sum.name == months) {
                        dataMonth.value = sum.value;
                    }
                }
                monthSummary.series.push(dataMonth);
            }
            summaryStatus.month.push(monthSummary);

            //ประจำสัปดาห์

            sqlWeek = "";

            sqlWeek = "";
            sqlWeek += " SELECT MONTH(updateDate) name, COUNT(updateDate) value, DAYOFWEEK(DATE(updateDate)) date FROM trdebtcollection";
            sqlWeek += " WHERE active = 'Y' AND DATE(updateDate) BETWEEN '"+firstday+"' AND '"+lastday+"' ";
            sqlWeek += " AND status = '" + obj.statusName + "'";
            sqlWeek += " GROUP BY DATE(updateDate);"
            let dashboardWeek = await db_sql.query(sqlWeek, { type: Sequelize.QueryTypes.SELECT });

            let weekSummary = {};
            weekSummary.name = obj.statusName;
            weekSummary.series = [];

            for (let i = 0; i < day.length; i++) {
                let dataWeek = {};
                dataWeek.name = day[i];
                dataWeek.value = 0;
                for (let sum of dashboardWeek) {
                    if (+sum.date === i + 1) {
                        dataWeek.value = sum.value;
                    }
                }
                weekSummary.series.push(dataWeek);
            }
            summaryStatus.week.push(weekSummary);

            //summary by updateDate
        }

        // year
        sql = "";
        sql += " SELECT MONTH(updateDate) name, COUNT(updateDate) value FROM trdebtcollection";
        sql += " WHERE active = 'Y' AND YEAR(updateDate) = '" + year + "' ";
        sql += " GROUP BY MONTH(updateDate);"
        let dataYear = await db_sql.query(sql, { type: Sequelize.QueryTypes.SELECT });

        let yearByupdateDate = {};
        yearByupdateDate.name = year;
        yearByupdateDate.series = [];

        for (let i = 0; i < month.length; i++) {
            let months = i + 1;
            let dataMonth = {};
            dataMonth.name = month[i];
            dataMonth.value = 0;

            for (let sum of dataYear) {
                if (sum.name == months) {
                    dataMonth.value = sum.value;
                }
            }
            yearByupdateDate.series.push(dataMonth);

        }

        summaryStatus.summaryYear.push(yearByupdateDate);

        // month
        sqlMonthSum = "";
        sqlMonthSum += " SELECT DAY(updateDate) name, COUNT(updateDate) value FROM trdebtcollection";
        sqlMonthSum += " WHERE active = 'Y' AND YEAR(updateDate) = '" + year + "' AND MONTH(updateDate) = '"+months+"' " ;
        sqlMonthSum += " GROUP BY DAY(updateDate)"
        let dataMonthSum = await db_sql.query(sqlMonthSum, { type: Sequelize.QueryTypes.SELECT });

        let monthByupdateDate = {};
        monthByupdateDate.name = year;
        monthByupdateDate.series = [];

        for (let i = 0; i < date; i++) {
            let months = i + 1;
            let dataDate = {};
            dataDate.name = i + 1;
            dataDate.value = 0;

            for (let sum of dataMonthSum) {
                if (sum.name == months) {
                    dataDate.value = sum.value;
                }
            }
            monthByupdateDate.series.push(dataDate);

        }

        summaryStatus.summaryMonth.push(monthByupdateDate);
        //week
        sqlWeekSum = "";
        sqlWeekSum += " SELECT COUNT(updateDate) value, DAYOFWEEK(DATE(updateDate)) date ";
        sqlWeekSum += " FROM trdebtcollection" ;
        sqlWeekSum += " WHERE active = 'Y' " ;
        sqlWeekSum += " AND  DATE(updateDate) BETWEEN '"+firstday+"' AND '"+lastday+"' " ;
        sqlWeekSum += " GROUP BY DATE(updateDate)" ;
        
        let dataWeekSum = await db_sql.query(sqlWeekSum, { type: Sequelize.QueryTypes.SELECT });

        let weekByupdateDate = {};
        weekByupdateDate.name = year;
        weekByupdateDate.series = [];

        for (let i = 0; i < day.length; i++) {
            let dataWeek = {};
            dataWeek.name = day[i];
            dataWeek.value = 0;
            for (let sum of dataWeekSum) {
                if (+sum.date === i + 1) {
                    dataWeek.value = sum.value;
                }
            }
            weekByupdateDate.series.push(dataWeek);

        }

        summaryStatus.summaryWeek.push(weekByupdateDate);
        //end

        serviceResult.value = summaryStatus;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);

    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }


}

module.exports = { findAll, findByPk, findAllTrace, findMyTrace, findByStatus, dashboardSummary };