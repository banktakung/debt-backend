var serviceResult = require('../../model/serviceResult');
var db_sql = require('../../configuration/database/db_sql');
const Sequelize = require('sequelize');
const log = require('../../configuration/log/log');
const Op = Sequelize.Op;
const dbManager = require('../../service/dbManager');
const syuser = require('../../model/syuser');
const saltedMd5 = require('salted-md5');
const config = require('../../configuration/config');


const save = async (req, res, next) => {
    try {
        var data = req.body;

        if (data.password) data.password = saltedMd5(data.password, config.app.passwordSalt);
        if (!data.createdBy) {
            data.createdBy = data.username;
            data.createDate = new Date();
        }
        data.updatedBy = data.username;
        data.updateDate = new Date();

        var where = { username: (data['username']) ? data['username'] : null };
        await dbManager.createOrUpdate(syuser, data, where).then(result => {
            serviceResult.value = result;
            serviceResult.code = 200;
            serviceResult.status = "Success";
            serviceResult.text = "Success";
            res.json(serviceResult);
        }).catch(err => console.error(err));
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

module.exports = { save }