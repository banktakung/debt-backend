var serviceResult = require('../../model/serviceResult');
var db_sql = require('../../configuration/database/db_sql');
const Sequelize = require('sequelize');
const log = require('../../configuration/log/log');
const mscustomercontact = require('../../model/mscustomercontact');
const Op = Sequelize.Op;

const findAll = async (req, res, next) => {
    try {
        let result = await mscustomercontact.findAll();
        serviceResult.value = result;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const findByPk = async (req, res, next) => {
    try {
        let value = req.query.value;
        let result = await mscustomercontact.findByPk(value);
        serviceResult.value = result;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const findByIdCard = async (req, res, next) => {
    try {
        let idcard = req.query.idcard;
        const result = await mscustomercontact.findAll({
            where: {
                idcard: idcard
            }
        }).catch(err => {
            console.log("code : cdfeccfc-ccf8-4d0c-b5a0-98d229f036dc");
            throw err;
        });
        serviceResult.value = result;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const getDebtContact = async (req, res, next) => {
    try {
        let companyContractNumber = req.query.companyContractNumber;

        let sql = "";
        sql += " SELECT";
        sql += " 	*,";
        sql += " 	( SELECT GROUP_CONCAT( mscustomercontact.phoneNumber, '|', mscustomercontact.status, '' ) FROM mscustomercontact WHERE mscustomercontact.idcard = tdr.idcard ) AS phone ";
        sql += " FROM";
        sql += " 	trdebtrelation tdr ";
        sql += " INNER JOIN mscustomer ON mscustomer.idcard = tdr.idcard"
        sql += " WHERE";
        sql += " 	tdr.active = 'Y' ";
        sql += " 	AND tdr.companyContractNumber = '"+ companyContractNumber +"'";
        const result = await db_sql.query(sql, { type: db_sql.QueryTypes.SELECT }).catch(err => {
            console.error("code : e73956b5-b0f9-4275-a5e6-ea669a789319"); throw err;
        });

        serviceResult.value = result;
        serviceResult.code = 200;
        serviceResult.status = "Success";
        serviceResult.text = "Success";
        res.json(serviceResult);
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

module.exports = { findAll, findByPk, findByIdCard, getDebtContact };