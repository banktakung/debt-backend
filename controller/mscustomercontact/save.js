var serviceResult = require('../../model/serviceResult');
var db_sql = require('../../configuration/database/db_sql');
const Sequelize = require('sequelize');
const log = require('../../configuration/log/log');
const Op = Sequelize.Op;
const dbManager = require('../../service/dbManager');
const mscustomercontact = require('../../model/mscustomercontact');


const save = async (req, res, next) => {
    try {
        var data = req.body;
        var where = { phoneId: (data['phoneId']) ? data['phoneId'] : 0 };
        await dbManager.createOrUpdate(mscustomercontact, data, where).then(result => {
            serviceResult.value = result;
            serviceResult.code = 200;
            serviceResult.status = "Success";
            serviceResult.text = "Success";
            res.json(serviceResult);
        }).catch(err => console.error(err));
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

const saveByPhoneNumber = async (req, res, next) => {
    try {
        var data = req.body;
        var where = { phoneNumber: (data['phoneNumber']) ? data['phoneNumber'] : 0 };
        await dbManager.createOrUpdate(mscustomercontact, data, where).then(result => {
            serviceResult.value = result;
            serviceResult.code = 200;
            serviceResult.status = "Success";
            serviceResult.text = "Success";
            res.json(serviceResult);
        }).catch(err => console.error(err));
    } catch (err) {
        console.error(err);
        log.error(err.stack);
        serviceResult.code = 500;
        serviceResult.status = "Error";
        serviceResult.text = "Error: " + err.message;
        res.json(serviceResult);
    }
}

module.exports = { save, saveByPhoneNumber }